/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TableModel;


import Data.Book;
import Data.Borrow;
import Data.Data;
import static Data.Data.booklist;
import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author c3356
 */
public class BorrowTableModel extends AbstractTableModel {
String[] columnName ={"bookid","Userid","BorrowTime"};
   ArrayList<Borrow> borrowlist  =Data.borrowlist;


 @Override
    public String  getColumnName(int column) {
        return columnName[column];
    }

    @Override
    public int getRowCount() {
     return borrowlist.size();
    }

    @Override
    public int getColumnCount() {
     return columnName.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
      Borrow borrow =borrowlist.get(rowIndex);
      if(borrow==null){
          return" ";
      }
      switch(columnIndex){
        case 0 : return borrow.getBookid();
        case 1 : return borrow.getUserid();
        case 2 : return borrow.getBorrowtime();
                  
     
    
    }
      return " ";
 }
}
