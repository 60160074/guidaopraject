/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TableModel;


import Data.Data;
import Data.User;
import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author c3356
 */
public class UserTableModel extends AbstractTableModel {
String[] columnName ={"id","username"};
   ArrayList<User> userlist  =Data.userlist;


 @Override
    public String  getColumnName(int column) {
        return columnName[column];
    }

    @Override
    public int getRowCount() {
     return userlist.size();
    }

    @Override
    public int getColumnCount() {
     return columnName.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
      User user =userlist.get(rowIndex);
      if(user==null){
          return" ";
      }
      switch(columnIndex){
        case 0 : return user.getUsername();
        case 1 : return user.getUserId();
     
    
    }
      return " ";
 }
}
