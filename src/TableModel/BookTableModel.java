/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TableModel;


import Data.Book;
import Data.Data;
import static Data.Data.booklist;
import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author c3356
 */
public class BookTableModel extends AbstractTableModel {
String[] columnName ={"bookid","bookname"};
   ArrayList<Book> booklist  =Data.booklist;


 @Override
    public String  getColumnName(int column) {
        return columnName[column];
    }

    @Override
    public int getRowCount() {
     return booklist.size();
    }

    @Override
    public int getColumnCount() {
     return columnName.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
      Book book =booklist.get(rowIndex);
      if(book==null){
          return" ";
      }
      switch(columnIndex){
        case 0 : return book.getBookname();
        case 1 : return book.getId();
     
    
    }
      return " ";
 }
}
