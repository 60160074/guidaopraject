/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Data;

import com.sun.istack.internal.logging.Logger;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JOptionPane;

/**
 *
 * @author c3356
 */
public class Db {

    public static Connection conn = null;
    public static String url = "jdbc:sqlite:.\\Database\\ContactData.db";

    public static Connection connect() {
        if (conn != null) {
            return conn;
        }
        /**
         * Connect to a sample database
         */
        try {
            // db parameters

            // create a connection to the database
            conn = DriverManager.getConnection(url);
            return conn;

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {

        }
        return null;

    }

    public static void disConnect() throws SQLException {

        try {
            if (conn != null) {
                conn.close();
                conn=null;
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
    }

}
