/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import Data.Book;
import Data.Data;
import Data.Db;
import Data.Log;
import Data.User;
import static dao.UserDao.currentid;
import static dao.UserDao.kb;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author c3356
 */
public class BookDao {
  static String current;
    public static ArrayList<Book> getBook() throws SQLException {
      
        ArrayList<Book> list = Data.booklist;
  
        try {
            Connection con = Db.connect();
            Statement stm = con.createStatement();
            String sql = "SELECT bookId,\n" +
"       name\n" +
"  FROM book;" 
;
            ResultSet rs = stm.executeQuery(sql);

            while (rs.next()) {

              Book book = ToObject(rs);
             if(list.size()!=0){
                
            }
              list.add(book);
            }

           
            Db.disConnect();
            return list;

        } catch (SQLException ex) {

        }
        return list;
    } public static void insertBook(String name) throws SQLException {
ArrayList<Book> list = Data.booklist;
        try {
            Connection con = Db.connect();
            Statement stm = con.createStatement();
            String sql = "INSERT INTO book (\n"+"name\n" +")\n" +"VALUES (\n" +"'"+name+"'\n" +");" ;    stm.execute(sql);
           
  System.out.print("insersuccess");
            newBook(name, list);
  
 
Db.disConnect();
        } catch (SQLException ex) {
            System.out.print(ex);
        }
    }

    private static void newBook(String name, ArrayList<Book> list) {
        Book book = new Book();
        book.setBookname(name);
        for(int i = 0;i<list.size();i++){
            if(i==list.size()-1){
                int  bookid = list.get(i).getId();
                book.setId(bookid+1);
                list.add(book);
                break;
            }
            
        } }

    private static  Book  ToObject(ResultSet rs) throws SQLException {
         Book book =  new Book();
      book.setBookname(rs.getString("name"));
      book.setId(rs.getInt("bookId"));
        return book;
    }  public static void removeBook(String name) throws SQLException {
 
        System.out.print("Are you sure to remove This Book"+name+"?(y/n)");
        String confrim = kb.next();
       if (confrim.equals("Y")||confrim.equals("y")||confrim.equals("yes")){
           try {
            Connection con = Db.connect();
            Statement stm = con.createStatement();
            String sql = "DELETE FROM book\n"
                    + "      WHERE name ='" + name + "';";
            stm.execute(sql);
         System.out.println("remove book Success!");
Db.disConnect();
        } catch (SQLException ex) {
            System.out.print(ex);
        }
       }else{
                   System.out.print("dont do anything");

       }
         
        
    }
}
