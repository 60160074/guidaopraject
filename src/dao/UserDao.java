/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import  Data.User;
import Data.Data;
import Data.Db;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Scanner;
import javax.swing.JOptionPane;

/**
 *
 * @author c3356
 */
public class UserDao {
  public static Scanner  kb  = new Scanner(System.in);
    public static String password, username, name, weight, height, surname;
    public static int typeId;
    public static int currentid;
    private Object Database;

    public static ArrayList<User> getUser() throws SQLException {
        ArrayList<User> list = Data.userlist;

        try {
            Connection con = Db.connect();
            Statement stm = con.createStatement();
            String sql = "SELECT userId,\n"
                    + "       loginName,\n"
                    + "       password,\n"
                    + "       name,\n"
                    + "       surname,\n"
                    + "       typeId,\n"
                    + "       weight,\n"
                    + "       height\n"
                    + "  FROM Users\n"
                    + "";
            ResultSet rs = stm.executeQuery(sql);

            while (rs.next()) {

                User user = toObject(rs);
                list.add(user);

            }

            currentid = list.get(0).getUserId();
            Db.disConnect();
            return list;

        } catch (SQLException ex) {

        }
        return list;
    }

    public static void saveUser(String username, String password, String name, String surname, int typeId, int weight, int height) throws SQLException {

        try {
            Connection con = Db.connect();
            Statement stm = con.createStatement();
            String sql = "INSERT INTO users (\n"
                    + "name,\n" + "surname,\n" + "loginName,\n" + "password,\n" + "weight,\n" + "height,\n" + "typeId\n" + ")\n"
                    + " VALUES (\n" + "'name',\n" + "'" + surname + "',\n" + "'" + username + "',\n" + "'" + password + "',\n" + "'" + weight + "',\n" + "'" + height + "',\n" + "'" + typeId + "'\n" + ");";
            stm.execute(sql);
           

            int userid = currentid + 1;

            User user = new User(userid, username, password, name, surname, typeId, weight, height);
            Data.userlist.add(user);
            System.out.println(Data.userlist.get(0).getName());

        } catch (SQLException ex) {
            System.out.print(ex);
        }
    }

    private static User toObject(ResultSet rs) throws SQLException {
        User user = new User();
       user.setUserid(Integer.parseInt(rs.getString("userId")));
        user.setUsername(rs.getString("loginName"));
        user.setPassword(rs.getString("password"));;
        user.setName(rs.getString("name"), rs.getString("surname"));
        int Type = Integer.parseInt(rs.getString("typeId"));
        int weight = Integer.parseInt(rs.getString("weight"));
        int height = Integer.parseInt(rs.getString("height"));
        user.setTypeId(Type);
        user.setWeight(weight);
        user.setHeight(height);
        return user;

    }

    public static void deleteUser(String username) throws SQLException {
 
        System.out.print("Are you sure to Delete This Users "+username+"?(y/n)");
        String confrim = kb.next();
       if (confrim.equals("Y")||confrim.equals("y")||confrim.equals("yes")){
           try {
            Connection con = Db.connect();
            Statement stm = con.createStatement();
            String sql = "DELETE FROM users\n"
                    + "      WHERE loginName ='" + username + "';";
            stm.execute(sql);
         System.out.print("Delete User Success!");

        } catch (SQLException ex) {
            System.out.print(ex);
        }
       }else{
                   System.out.print("Data not change");

       }
         
        
    } public static void UpdateUser(String username, String password, String name, String surname, int typeId, int weight, int height,int userid) throws SQLException {

        try {
            Connection con = Db.connect();
            Statement stm = con.createStatement();
            String sql = "UPDATE users\n" +
"   SET name = '"+name+"',\n" +
"       surname = '"+surname+"',\n" +
"       loginName = '"+username+"',\n" +
"       password = '"+password+"',\n" +
"       weight = "+weight+",\n" +
"       height = "+height+",\n" +
"       typeId = "+typeId+"\n" +
" WHERE  userId = '"+userid+"'" ;  
            stm.execute(sql);
                    
           
JOptionPane.showMessageDialog(null,"Update Data for userid  :"+userid+"  success");
           

            
        } catch (SQLException ex) {
            System.out.print(ex);
        }
    }

}
